package com.greatlearning.assignment9.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.assignment9.bean.Book;
import com.greatlearning.assignment9.bean.Users;
import com.greatlearning.assignment9.dao.UsersDao;

@Service
public class UsersService {

	@Autowired
	UsersDao usersDao;
	
	public List<Users> getAllUsers(){
  	  return usersDao.findAll();
    }
    
    public String storeUsersInfo(Users u) {
  	  if(usersDao.existsById(u.getId())) {
  		  return "enter valid id";
  	  }else {
  		  usersDao.save(u);
  		  return "users information saved successfully";
  	  }
    }
    
    public String deleteUsersInfo(int email) {
  	  if(!usersDao.existsById(email)){
  		  return "email id not present";
  	  }else {
  		  usersDao.deleteById(email);
  		  return "email deleted successfully";
  	  }
    }
    
    public String updateUsersInfo(Users u) {
  	  if(!usersDao.existsById(u.getId())){
  		  return "enter valid id";
  	  }else {
  		  Users users=usersDao.getById(u.getId());
  		  users.setPassword(u.getPassword());
  		  usersDao.saveAndFlush(users);
  		  return "password updated successfully";
  	  }
    }
}
