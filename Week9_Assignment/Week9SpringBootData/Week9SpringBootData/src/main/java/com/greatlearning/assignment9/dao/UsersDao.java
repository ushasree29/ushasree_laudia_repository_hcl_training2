package com.greatlearning.assignment9.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.greatlearning.assignment9.bean.Users;

@Repository
public interface UsersDao extends JpaRepository<Users, Integer>{

}
