package com.greatlearning.assignment9.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.greatlearning.assignment9.bean.Book;
import com.greatlearning.assignment9.service.BookService;

@RestController
@RequestMapping("/book")
public class BookController {

	  @Autowired
	  BookService bookService;
	  
	  @GetMapping(value="getAllBook",produces = MediaType.APPLICATION_JSON_VALUE)
		 public List<Book> getAllProductInfo(){
			 return bookService.getAllBooks();
		 }
		 
		 @PostMapping(value="storeBook",consumes = MediaType.APPLICATION_JSON_VALUE)
		 public String storeProduct(@RequestBody Book b) {
			 return bookService.storeBookInfo(b);
		 }
		 
		 @DeleteMapping(value="deleteBook/{id}")
		 public String deleteBooks(@PathVariable("id")int id) {
			 return bookService.deleteBookInfo(id);
		 }
		 
		 @PatchMapping(value="updateBook")
		 public String updateProduct(@RequestBody Book b) {
			 return bookService.updateBookInfo(b);
		 }
}
