package com.greatlearning.assignment9.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "com.greatlearning.assignment9")
@EntityScan(basePackages = "com.greatlearning.assignment9.bean")
@EnableJpaRepositories(basePackages = "com.greatlearning.assignment9.dao")
public class Week9SpringBootDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(Week9SpringBootDataApplication.class, args);
		System.out.println("Server is running on port number 9090");
	}

}
