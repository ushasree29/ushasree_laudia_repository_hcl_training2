package com.greatlearning.assignment9.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Users {
	
@Id
@Column(name="id")
private int id;
@Column(name="emailid")
private String emailId;
private String username;
private String password;

public Users() {
	super();
	// TODO Auto-generated constructor stub
}

public Users(int id,String emailId, String username, String password) {
	super();
	this.id=id;
	this.emailId = emailId;
	this.username = username;
	this.password = password;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getEmailId() {
	return emailId;
}
public void setEmailId(String emailId) {
	this.emailId = emailId;
}
public String getUsername() {
	return username;
}
public void setUsername(String username) {
	this.username = username;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}

@Override
public String toString() {
	return "Users [id=" + id + ", emailId=" + emailId + ", username=" + username + ", password=" + password + "]";
}


}
