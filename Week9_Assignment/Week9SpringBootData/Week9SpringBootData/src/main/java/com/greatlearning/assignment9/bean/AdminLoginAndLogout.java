package com.greatlearning.assignment9.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="adminloginandlogout")
public class AdminLoginAndLogout {
@Id
@Column(name="id")
private int id;
@Column(name="emailid")
private String emailId;
private String password;


public AdminLoginAndLogout() {
	super();
	// TODO Auto-generated constructor stub
}

public AdminLoginAndLogout(int id, String emailId, String password) {
	super();
	this.id = id;
	this.emailId = emailId;
	this.password = password;
}

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getEmailId() {
	return emailId;
}
public void setEmailId(String emailId) {
	this.emailId = emailId;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
@Override
public String toString() {
	return "AdminLoginAndLogout [id=" + id + ", emailId=" + emailId + ", password=" + password + "]";
}


}
