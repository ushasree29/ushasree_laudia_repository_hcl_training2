package com.greatlearning.assignment9.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.assignment9.bean.UsersLikedBooks;

@Repository
public interface UsersLikedBooksDao extends JpaRepository<UsersLikedBooks, Integer> {

}
