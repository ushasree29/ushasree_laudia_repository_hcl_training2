package com.greatlearning.assignment9.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.assignment9.bean.Book;
import com.greatlearning.assignment9.dao.BookDao;

@Service
public class BookService {
          @Autowired
          BookDao bookDao;
          
          public List<Book> getAllBooks(){
        	  return bookDao.findAll();
          }
          
          public String storeBookInfo(Book b) {
        	  if(bookDao.existsById(b.getId())) {
        		  return "book id should be unique";
        	  }else {
        		  bookDao.save(b);
        		  return "book saved successfully";
        	  }
          }
          
          public String deleteBookInfo(int id) {
        	  if(!bookDao.existsById(id)){
        		  return "book id not present";
        	  }else {
        		  bookDao.deleteById(id);
        		  return "book deleted successfully";
        	  }
          }
          
          public String updateBookInfo(Book b) {
        	  if(!bookDao.existsById(b.getId())){
        		  return "book id not present";
        	  }else {
        		  Book book=bookDao.getById(b.getId());
        		  book.setPrice(b.getPrice());
        		  bookDao.saveAndFlush(book);
        		  return "book updated successfully";
        	  }
          }
}
