package com.greatlearning.assignment9.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.assignment9.bean.AdminSignUp;
import com.greatlearning.assignment9.bean.Users;
import com.greatlearning.assignment9.dao.AdminSignUpDao;

@Service
public class AdminSignUpService {
	@Autowired
	AdminSignUpDao adminsignupdao;

	public List<AdminSignUp> getAllAdmin(){
	  	  return adminsignupdao.findAll();
	    }
	    
	    public String storeAdminInfo(AdminSignUp ad) {
	  	  if(adminsignupdao.existsById(ad.getId())) {
	  		  return "enter valid id";
	  	  }else {
	  		adminsignupdao.save(ad);
	  		  return "admin information saved successfully";
	  	  }
	    }
}
