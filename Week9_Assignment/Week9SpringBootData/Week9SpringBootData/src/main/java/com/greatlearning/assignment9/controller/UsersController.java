package com.greatlearning.assignment9.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.assignment9.bean.Book;
import com.greatlearning.assignment9.bean.Users;
import com.greatlearning.assignment9.service.UsersService;

@RestController
@RequestMapping("/users")
public class UsersController {

	@Autowired
	UsersService usersService;
	
	@GetMapping(value="getAllUsers",produces = MediaType.APPLICATION_JSON_VALUE)
	 public List<Users> getAllUsersInfo(){
		 return usersService.getAllUsers();
	 }
	 
	 @PostMapping(value="storeUsers",consumes = MediaType.APPLICATION_JSON_VALUE)
	 public String storeUsers(@RequestBody Users u) {
		 return usersService.storeUsersInfo(u);
	 }
	 
	 @DeleteMapping(value="deleteUsers/{id}")
	 public String deleteUsers(@PathVariable("id")int id) {
		 return usersService.deleteUsersInfo(id);
	 }
	 
	 @PatchMapping(value="updateUsers")
	 public String updateUsers(@RequestBody Users u) {
		 return usersService.updateUsersInfo(u);
	 }
}
