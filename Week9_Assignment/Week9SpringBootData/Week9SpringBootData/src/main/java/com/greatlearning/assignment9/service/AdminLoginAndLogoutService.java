package com.greatlearning.assignment9.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.assignment9.bean.AdminLoginAndLogout;
import com.greatlearning.assignment9.bean.AdminSignUp;
import com.greatlearning.assignment9.dao.AdminLoginAndLogoutDao;

@Service
public class AdminLoginAndLogoutService {

	@Autowired
	AdminLoginAndLogoutDao adminloginandlogoutDao;
	
	public List<AdminLoginAndLogout> getAllAdmin(){
	  	  return adminloginandlogoutDao.findAll();
	    }
	    
	    public String storeAdminInfo(AdminLoginAndLogout ad) {
	  	  if(adminloginandlogoutDao.existsById(ad.getId())) {
	  		  return "enter valid id";
	  	  }else {
	  		adminloginandlogoutDao.save(ad);
	  		  return "admin login information saved successfully";
	  	  }
	    }
	    public String deleteLoginInfo(int id) {
      	  if(!adminloginandlogoutDao.existsById(id)){
      		  return "login id not present enter correct id";
      	  }else {
      		adminloginandlogoutDao.deleteById(id);
      		  return "user deleted successfully and Logged out as well..!!!";
      	  }
        }
}
