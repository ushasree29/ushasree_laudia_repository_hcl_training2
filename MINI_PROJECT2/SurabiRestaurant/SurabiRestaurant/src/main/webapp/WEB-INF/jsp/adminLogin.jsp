<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin Log In</title>

<style type="text/css">
body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    } 
#header {
        background-color: #16a085;
        color: #fff;
    }
h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    }                
</style>
</head>


<body>
	<%
	Object loginResult = request.getAttribute("objLogInResult");
	if (loginResult != null) {
		out.print(loginResult);
	}
	%>
	<div align="center">
		<h1>SURABI RESTAURANT</h1>
		<hr>
	<h2>Admin Login</h2>
	<form action="/admin/adminLogin" method="post">
		<label> E-Mail &nbsp &nbsp</label> <input type="email" name="email" required />
		<br />
		<br /> <label>Password </label> <input type="password" name="password"
			required><br />
		<br /> <input type="submit" value="Submit" class ="btn"> 
		<br />
		<hr>

	</form>
	</div>
</body>
</html>