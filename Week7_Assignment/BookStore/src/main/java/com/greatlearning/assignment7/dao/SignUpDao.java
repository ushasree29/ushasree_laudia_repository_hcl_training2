package com.greatlearning.assignment7.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import com.greatlearning.assignment7.bean.SignUp;


public class SignUpDao {
        private String url="jdbc:mysql://localhost:3306/bookstore_ushasree";
        private String name="root";
        private String pass="Usha99**";
        private String driver="com.mysql.cj.jdbc.Driver";
        public void DbLoadDriver(String driver) {
        	try {
       		 Class.forName(driver);
       		 
       	}catch(Exception e) {
       		System.out.println(" "+e);
       	}
        }
        
        public Connection getConnection() {
        	 Connection con=null;
        	try {
        		
       		    con=DriverManager.getConnection(url,name,pass);
       		    
        		
        	}catch(Exception e) {
        		System.out.println("Exception caught"+e);
        	}
        	return con;
        }
	public String storeSignUp(SignUp sp) {
		DbLoadDriver(driver);
		Connection con = getConnection();
		String result="data stored successfully";
		try {
			
			PreparedStatement pstmt = con.prepareStatement("insert into registration values(?,?,?,?,?,?)");
			pstmt.setString(1, sp.getFirst_name());
			pstmt.setString(2,sp.getLast_name());
			pstmt.setString(3, sp.getEmailId());
			pstmt.setString(4,sp.getPassword());
			pstmt.setString(5,sp.getAddress());
			pstmt.setString(6,sp.getGender());
			pstmt.executeUpdate();
			
		} catch (Exception e) {
			System.out.println("Store Method Exception "+e);
			result="data didn't store";
		}
		return result;
	}
}
