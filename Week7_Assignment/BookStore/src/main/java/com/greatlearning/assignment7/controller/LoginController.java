package com.greatlearning.assignment7.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.greatlearning.assignment7.bean.Book;
import com.greatlearning.assignment7.bean.Login;
import com.greatlearning.assignment7.dao.LoginDao;
import com.greatlearning.assignment7.service.BookService;



/**
 * Servlet implementation class LoginController
 */
@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
        Book book=new Book();
		
		BookService bs=new BookService();
		List<Book> res=bs.getAllBook();
		HttpSession hs=request.getSession();
		hs.setAttribute("obj",res);
	    RequestDispatcher req = request.getRequestDispatcher("dashboard.jsp");
		req.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		response.setContentType("text/html");
        PrintWriter pw=response.getWriter();
		String email=request.getParameter("emailId");
	    String pass=request.getParameter("password");
	    Login login=new Login(email,pass);
	    LoginDao ldao=new LoginDao();
	    String res=ldao.storeLoginInfo(login);
	    HttpSession hs=request.getSession();
	    hs.setAttribute("login",email);
	    response.getWriter().println(res);
	    RequestDispatcher rd=request.getRequestDispatcher("UserPage.jsp");	
	    rd.forward(request, response);
	    }

}
