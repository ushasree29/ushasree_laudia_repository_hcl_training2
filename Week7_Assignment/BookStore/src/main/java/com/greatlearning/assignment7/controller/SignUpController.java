package com.greatlearning.assignment7.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.greatlearning.assignment7.bean.Book;
import com.greatlearning.assignment7.bean.SignUp;
import com.greatlearning.assignment7.dao.SignUpDao;
import com.greatlearning.assignment7.service.BookService;


/**
 * Servlet implementation class SignUpController
 */
@WebServlet("/SignUpController")
public class SignUpController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignUpController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		response.setContentType("text/html");
		String first_name=request.getParameter("first_name");
		String last_name=request.getParameter("last_name");
		String emailId=request.getParameter("emailId");
		String password=request.getParameter("password");
		String address=request.getParameter("address");
		String gender=request.getParameter("gender");
		SignUp sp=new SignUp(first_name, last_name, emailId, password, address, gender);
		
		SignUpDao sdao=new SignUpDao();
		String res=sdao.storeSignUp(sp);
		response.getWriter().print(res);

	    RequestDispatcher rd=request.getRequestDispatcher("Login.jsp");
		rd.include(request,response);
		
	}

}
