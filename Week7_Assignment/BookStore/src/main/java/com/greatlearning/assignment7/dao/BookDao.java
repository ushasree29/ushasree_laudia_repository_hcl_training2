package com.greatlearning.assignment7.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.greatlearning.assignment7.bean.Book;
import com.greatlearning.assignment7.resource.DbResource;


public class BookDao {


	    public List<Book> fetchUrlFromBook(){
	    	List<Book> list=new ArrayList<>();
	    	try {
	    		Connection con=DbResource.getConnection();
				PreparedStatement pstmt = con.prepareStatement("select * from book");
				ResultSet rs = pstmt.executeQuery();
				  while(rs.next()) {
						Book p = new Book();
						p.setId(rs.getInt(1));
						p.setType(rs.getString("type"));
						p.setName(rs.getString("name"));
						p.setAuthor(rs.getString("author"));
						p.setImage(rs.getString("image"));
						list.add(p);
				   }
				}catch (Exception e) {
					System.out.println("In store method "+e);
				  }
			        return list;
	    	 }
	    
	    public Book getOne(int id) {
	    	List<Book> list=new ArrayList<>();
	    	try {
	    		Connection con=DbResource.getConnection();
				PreparedStatement pstmt = con.prepareStatement("select * from book where id="+id);
				Book p = new Book();
				ResultSet rs = pstmt.executeQuery();
				  while(rs.next()) {
						
						p.setId(rs.getInt(1));
						p.setType(rs.getString("type"));
						p.setName(rs.getString("name"));
						p.setAuthor(rs.getString("author"));
						p.setImage(rs.getString("image"));
						
				   }
				  return p;
				}catch (Exception e) {
					System.out.println("In store method "+e);
				  }
			        return null;
	    	 }
	    
	    
	
}
