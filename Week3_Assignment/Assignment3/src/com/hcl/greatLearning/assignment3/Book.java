package com.hcl.greatLearning.assignment3;

public class Book {
	private int id;
	private	String name;
	private	Double price;
	private	int noOfCopiesSold;
	protected String bookstatus;
	protected String genre;
	
	public Book() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Book( int id,String name, Double price, int noOfCopiesSold, String genre,String bookstatus){
		super();
		   
	    this.id = id; 
		this.name = name;
		this.price = price;
		this.noOfCopiesSold = noOfCopiesSold;
		this.bookstatus = bookstatus;
		this.genre = genre;
		   

	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public int getNoOfCopiesSold() {
		return noOfCopiesSold;
	}
	public void setNoOfCopiesSold(int noOfCopiesSold) {
		this.noOfCopiesSold = noOfCopiesSold;
	}
	public String getBookstatus() {
		return bookstatus;
	}
	public void setBookstatus(String bookstatus) {
		this.bookstatus = bookstatus;
	}
		
	@Override
	public String toString() {
		return "Book [ id=" + id + ", name=" + name + ", price=" + price + ", genre=" + genre + ", noOfCopiesSold="
					+ noOfCopiesSold +  ", bookstatus=" + bookstatus +"]";
	}
}
