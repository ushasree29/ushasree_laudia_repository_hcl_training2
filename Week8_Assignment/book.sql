
create database bookstore_ushasree;

use bookstore_ushasree;

create table book(id int primary key,name varchar(100),author varchar(50),type varchar(30),image varchar(500),price float);

insert into book values(1,"5G Mobile and Wireless Communications Technology","Afif Osseiran","Technology","https://m.media-amazon.com/images/I/61A9na2f1ZL._AC_UY327_FMwebp_QL65_.jpg",4050);
insert into book values(2,"Study Guide to AFCAT 2021","Disha Experts","Photography","https://th.bing.com/th/id/OIP.E_mlMjB0IXmXKrSgbF1aIQAAAA?w=203&h=266&c=7&r=0&o=5&dpr=1.5&pid=1.7",3400);
insert into book values(3,"Harry Potter","R. K. Rowling","Adventure & Friction","https://m.media-amazon.com/images/I/51UoqRAxwEL.jpg",3200);
insert into book values(4,"The Diary of a Young Girl","Anne Frank","autobiography","https://m.media-amazon.com/images/I/81lZ-9E4F-S._AC_UY327_FMwebp_QL65_.jpg",6000);
insert into book values(5,"The Psychology of Money","Morgan Housel","pschology", "https://m.media-amazon.com/images/I/71g2ednj0JL._AC_UY327_FMwebp_QL65_.jpg",2400);
insert into book values(6,"Cricket Quiz Book","Kalyan B. Bhattacharyya","Sports","https://m.media-amazon.com/images/I/71YHI2X7HmL._AC_UY327_FMwebp_QL65_.jpg",2340);
insert into book values(7,"Operating Systems","R. Menaka","Engineering","https://m.media-amazon.com/images/I/813WA8kiCUL._AC_UY327_FMwebp_QL65_.jpg",2350);
insert into book values(8,"The Official Cambridge Guide To Ielts","A.K. Jesse","Exam Preparation","https://m.media-amazon.com/images/I/91oc6Hcx1ML._AC_UY327_FMwebp_QL65_.jpg",4450);
insert into book values(9,"A Glimpse of the Universe","Mahatma Ramratna Thapaliyal","Science & Friction","https://m.media-amazon.com/images/I/91wUFiHq0zL._AC_UY327_FMwebp_QL65_.jpg",4090);
insert into book values(10,"Autopilot","Andrew Smart","Science & Friction","https://m.media-amazon.com/images/I/81zCqRFh+FL._AC_UY327_FMwebp_QL65_.jpg",1320);
insert into book values(11,"Psychopathology of Everyday Life","Sigmund Freud","pschology","https://m.media-amazon.com/images/I/71f2MpngVrL._AC_UY327_FMwebp_QL65_.jpg",670);
insert into book values(12,"BUDDHA: Spirituality For Leadership & Success","Pranay","Spirituality","https://m.media-amazon.com/images/I/81damewvwxS._AC_UY327_FMwebp_QL65_.jpg",2380);
insert into book values(13,"Travelogue Across Hemispheres","Bhupender Gupta","Travel Guide","https://m.media-amazon.com/images/I/71FQ5qET49L._AC_UY327_FMwebp_QL65_.jpg",6734);
insert into book values(14,"Wild Weather National Geographic","R. Rowling","geographic","https://m.media-amazon.com/images/I/41W5H76a6ZS._AC_UY327_FMwebp_QL65_.jpg",900);
insert into book values(15,"Book Of Common Signs","Ashok Srinivasan","biography","https://m.media-amazon.com/images/I/712yz+57JDL._AC_UY327_FMwebp_QL65_.jpg",1000);
insert into book values(16,"Rich Dad Poor Dad","Robert T. Kiyosaki","Autobiography","https://m.media-amazon.com/images/I/81dQwQlmAXL._AC_UY327_QL65_.jpg",1200);




desc book;
select *from book;

create table login(emailId varchar(50) primary key,password varchar(20));

desc login;
select *from login;

create table registration(first_name varchar(30),last_name varchar(20),emailId varchar(40) primary key,password varchar(30),address varchar(80), contact int,gender varchar(10));

desc registration;
select *from registration;


    



