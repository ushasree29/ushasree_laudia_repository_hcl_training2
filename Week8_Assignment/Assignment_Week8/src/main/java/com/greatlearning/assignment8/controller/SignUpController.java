package com.greatlearning.assignment8.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.greatlearning.assignment8.service.SignUpService;
@Controller
public class SignUpController {
	@Autowired
	SignUpService signUpService;

	@RequestMapping(value = "SignUp",method = RequestMethod.GET)
	public ModelAndView storeLoginDetails(HttpServletRequest req,HttpSession hs) {	 
		hs.setAttribute("first_name", hs);
		hs.setAttribute("last_name",hs);
		hs.setAttribute("emailId",hs);
		hs.setAttribute("password", hs);
		hs.setAttribute("address",hs);
		hs.setAttribute("gender", hs);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("SignUp.jsp");
		return mav;
	}
}
