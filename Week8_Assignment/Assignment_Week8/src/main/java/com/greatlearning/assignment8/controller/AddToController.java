package com.greatlearning.assignment8.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.greatlearning.assignment8.bean.Book;
import com.greatlearning.assignment8.dao.BookDao;
import com.greatlearning.assignment8.service.BookService;

@Controller
public class AddToController {
     @Autowired
     BookService bookService;
     @GetMapping(value="cartItems")
     public ModelAndView getDetails(HttpServletRequest req) {
     ModelAndView mav=new ModelAndView();
     int id=Integer.parseInt(req.getParameter("id"));
     Book list=bookService.getOneById(id);
     req.setAttribute("books", list);
     req.setAttribute("obj", "");
     mav.setViewName("CartItems.jsp");
     return mav;
     }
}
