package com.greatlearning.assignment8.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.greatlearning.assignment8.bean.Login;
import com.greatlearning.assignment8.bean.SignUp;
@Repository
public class SignUpDao {

	@Autowired
    JdbcTemplate jdbcTemplate;
    
    public List<SignUp> findBookById(){
   	 return jdbcTemplate.query("select * from registration",new SignUpRowMapper());
    }
    
}
class SignUpRowMapper implements RowMapper<SignUp>{

@Override
public SignUp mapRow(ResultSet rs, int rowNum) throws SQLException {
	SignUp sp=new SignUp();
	sp.setFirst_name(rs.getString(1));
	sp.setLast_name(rs.getString(2));
	sp.setEmailId(rs.getString(3));
	sp.setPassword(rs.getString(4));
	sp.setAddress(rs.getString(5));
	sp.setGender(rs.getString(6));
	return sp;
  }
}
