package com.greatlearning.assignment8.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import com.mysql.cj.Query;
import com.greatlearning.assignment8.bean.Book;

@Repository
public class BookDao {
     @Autowired
     JdbcTemplate jdbcTemplate;
     
     public List<Book> getAllBooks(){
    	 return jdbcTemplate.query("select *from book", new BookRowMapper());
     }
     
     
     public Book getOne(int id) {
    	
    	 return (Book) jdbcTemplate.query("select * from book",new BookRowMapper());
     }
}
class BookRowMapper implements RowMapper<Book>{

	@Override
	public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
		Book b=new Book();
		b.setId(rs.getInt(1));
		b.setName(rs.getString(2));
		b.setAuthor(rs.getString(3));
		b.setType(rs.getString(4));
		b.setImage(rs.getString(5));
		return b;
	}
	
}
