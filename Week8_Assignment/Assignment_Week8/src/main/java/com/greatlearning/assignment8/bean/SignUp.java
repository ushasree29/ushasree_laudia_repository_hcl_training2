package com.greatlearning.assignment8.bean;

public class SignUp {
private String first_name;
private String last_name;
private String emailId;
private String password;
private String address;
private String gender;
public String getFirst_name() {
	return first_name;
}
public void setFirst_name(String first_name) {
	this.first_name = first_name;
}
public String getLast_name() {
	return last_name;
}
public void setLast_name(String last_name) {
	this.last_name = last_name;
}
public String getEmailId() {
	return emailId;
}
public void setEmailId(String emailId) {
	this.emailId = emailId;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
@Override
public String toString() {
	return "SignUp [first_name=" + first_name + ", last_name=" + last_name + ", emailId=" + emailId + ", password="
			+ password + ", address=" + address + ", gender=" + gender + "]";
}


}
