package com.greatlearning.assignment8.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.greatlearning.assignment8.bean.Book;
import com.greatlearning.assignment8.bean.Login;
import com.greatlearning.assignment8.service.BookService;
import com.greatlearning.assignment8.service.LoginService;

@Controller
public class LoginController {
         @Autowired
         LoginService loginService;
         BookService bookService;
         
         @RequestMapping(value="Login",method = RequestMethod.GET)
         public ModelAndView getAllLoginDetails(HttpServletRequest req,HttpSession hs) {
        	 ModelAndView mav=new ModelAndView();
        	 hs.setAttribute("emailId", hs);
        	 hs.setAttribute("password", hs);
        	
        	 req.setAttribute("books", hs);
        	 mav.setViewName("Login.jsp");
        	 return mav;
         }
}
