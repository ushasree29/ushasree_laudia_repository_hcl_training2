package com.greatlearning.assignment8.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.greatlearning.assignment8.bean.Book;
import com.greatlearning.assignment8.service.BookService;

@Controller
public class BookController {
           @Autowired
           BookService bookService;
           
           @GetMapping(value="dashboard")
           public ModelAndView getAllBookDetails(HttpServletRequest req) {
        	   ModelAndView mav=new ModelAndView();
        	   List<Book> list=bookService.getAllBooks();
        	   req.setAttribute("books", list);
        	   //req.setAttribute("obj","Book Details are");
        	   //out.println(request.getAttribute("obj"));
        	   mav.setViewName("dashboard.jsp");
        	   return mav;
           }
}
