package com.greatlearning.assignment8.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.greatlearning.assignment8.bean.Login;

@Repository
public class LoginDao {
         @Autowired
         JdbcTemplate jdbcTemplate;
         
         public List<Login> findBookById(){
        	 return jdbcTemplate.query("select * from login",new LoginRowMapper());
         }
         
}
class LoginRowMapper implements RowMapper<Login>{

	@Override
	public Login mapRow(ResultSet rs, int rowNum) throws SQLException {
		Login login=new Login();
		login.setEmailId(rs.getString(1));
		login.setPassword(rs.getString(2));
		return login;
	}
	
}
