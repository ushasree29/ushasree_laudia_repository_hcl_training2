<%@page import="com.greatlearning.assignment8.bean.Book"%>
<%@page import="com.greatlearning.assignment8.dao.BookDao"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ListIterator"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Items added to cart</title>
</head>
<body>
<h5>Items added to cart</h5>

<form action="AddToController" method="post"></form>
<div class="conatiner-fluid">
<!-- Book section -->
<table border="1">
	<tr>
			<th>S.No</th>
			<th>Id</th>
			<th>Name</th>
			<th>Author</th>
			
			
	</tr>

<%
   if(session.getAttribute("addToCart")!=null){
	   ListIterator list=((List)session.getAttribute("addToCart")).listIterator();
	   BookDao bd=new BookDao();
	   int a=0,total=0;
	   while(list.hasNext()){
		   a++;
	       int id=Integer.parseInt((String)list.next());
		   Book b=bd.getOne(id);
		   total++;
%>
		   <tr>
		        <td><%= a %></td>
		        <td><%= b.getId() %></td>
		        <td><%= b.getName() %></td>
		        <td><%= b.getAuthor() %></td>
		       
		   </tr>
		   <%
		       }
	       %>
	       <tr>
	       <td colspan="5"><strong>Total = <%= total%></strong></td>
	       <%
	          }
	       %>
	       </tr>
	       
	       <tr><td style="text-align:center" colspan="6"><button class="btn btn-success btn-lg"
	        onclick="location.href='Logout.jsp'">Logout</button>
	       </td></tr>
	       
</div>
</body>
</html>