-> create database travel_ushasree;

-> use travel_ushasree;


 ->create table passenger
  (Passenger_name     varchar(20), 
   Category           varchar(20),
   Gender             varchar(20),
   Boarding_City      varchar(20),
   Destination_City   varchar(20),
   Distance           int,
   Bus_Type           varchar(20)
   );

->insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
 insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
 insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
 insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
 insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
 insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
 insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
 insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
 insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');

->select *from passenger;


->create table price
  (Bus_Type   varchar(20),
   Distance   int,
   Price      int
   );

->insert into price values('Sleeper',350,770);
 insert into price values('Sleeper',500,1100);
 insert into price values('Sleeper',600,1320);
 insert into price values('Sleeper',700,1540);
 insert into price values('Sleeper',1000,2200);
 insert into price values('Sleeper',1200,2640);
 insert into price values('Sleeper',350,434);
 insert into price values('Sitting',500,620);
 insert into price values('Sitting',500,620);
 insert into price values('Sitting',600,744);
 insert into price values('Sitting',700,868);
 insert into price values('Sitting',1000,1240);
 insert into price values('Sitting',1200,1488);
 insert into price values('Sitting',1500,1860);
 
->select *from price;


1. SELECT gender,count(gender) FROM passenger WHERE distance>=600 GROUP BY gender;
   //

2. SELECT min(price) FROM price WHERE Bus_Type="Sleeper";
   //

3. SELECT Passenger_name FROM passenger WHERE Passenger_name LIKE 'S%';
   //

4. SELECT pgr.passenger_name, pgr.boarding_city, pgr.destination_city, pgr.bus_type, sum(pr.price) AS ticket_price FROM passenger pgr INNER JOIN price pr ON pgr.bus_type = pr.bus_type AND pgr.distance = pr.distance GROUP BY pgr.passenger_name;
   //

5. SELECT pgr.passenger_name, pr.price AS 'ticket price'FROM passenger pgr JOIN price pr ON pgr.bus_type = 'sitting' AND pgr.distance = 1000 GROUP BY pgr.passenger_name;
   //

6. SELECT pr.bus_type, pr.price FROM passenger pgr, price pr WHERE pgr.passenger_name = 'Pallavi' AND pgr.distance = pr.distance;
   //

7. SELECT DISTINCT(distance) FROM passenger ORDER BY distance DESC;
   // 

8. SELECT passenger_name, (distance /(Select SUM(distance) FROM passenger) * 100) AS 'Percentage%_of_Distance_travelled' FROM passenger;
   //

9. SELECT Passenger_name,Category FROM view_passenger;
   //

10.
delimiter //
create procedure sleeper_listpassenger()
begin
select passenger_name,Bus_Type from passenger where Bus_Type='Sleeper';
select count(*) from passenger where Bus_Type='Sleeper';
end//

call sleeper_listpassenger()//

11. Select *from passenger limit 5;
    //


