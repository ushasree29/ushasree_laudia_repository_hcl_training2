package com;
public class Employee implements Comparable<Employee>{
//Employee fields
private int id;
private String name;
private int age;
private int salary;
private String dept;
private String city;
	
//Empty constructor
public Employee() {
super();
// TODO Auto-generated constructor stub
}  

//Parameterized constructor 
public Employee(int id, String name, int age, int salary,String dept, String city) {
super();
this.id = id;
this.name = name;
this.age = age;
this.salary = salary;
this.dept = dept;
this.city = city;
}
 
//override compareTo method
@Override
public int compareTo(Employee o) {
    	
return this.name.compareTo(getName());
}

   
//Setter & Getter methods   
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public int getSalary() {
	return salary;
}
public void setSalary(int salary) {
	this.salary = salary;
}
public String getDept() {
	return dept;
}
public void setDept(String dept) {
	this.dept = dept;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}

//Override toString method
@Override
public String toString() {
	return name;
}
 
}
