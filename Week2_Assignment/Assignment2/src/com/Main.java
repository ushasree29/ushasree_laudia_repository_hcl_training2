package com;
import java.util.ArrayList;
import java.util.Collections;
public class Main {
public static void main(String[] args) {
		ArrayList<Employee> employees=new ArrayList<>();
		employees.add(new Employee(1,"Aman",20,1100000,"IT","Delhi"));
		employees.add(new Employee(2,"Bobby",22,500000,"HR","Bombay"));
		employees.add(new Employee(3,"Zoe",20,750000,"Admin","Delhi"));
		employees.add(new Employee(4,"Smitha",21,1000000,"IT","Chennai"));
		employees.add(new Employee(5,"Smitha",24,1200000,"HR","Bengaluru"));
		System.out.printf("%7s %14s %7s %18s %15s %13s", "S.No", "Name", "Age", "Salary", "Department", "City");
        System.out.println();
        System.out.println("--------------------------------------------------------------------------------");
        for(Employee employee:employees){
        	System.out.format("%7s %14s %7s %18s %15s %13s", employee.getId(), employee.getName(), employee.getAge(), employee.getSalary(), employee.getDept(), employee.getCity());
            System.out.println();
        }
        System.out.println("---------------------------------------------------------------------------------");
        
        DataStructureB b=new DataStructureB();
        System.out.println("Monthly salary id wise:");
        b.monthlySalary(employees);
	    System.out.println("\n");
	    
	    System.out.println("Employees residing in cities counts:");
	    b.cityNameCount(employees);
	    System.out.println("\n");
        
        System.out.println("Employee names in sorted order");
		Collections.sort(employees, new DataStructureA());
        System.out.println(employees);
		}
}
