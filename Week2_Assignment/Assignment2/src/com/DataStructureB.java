package com;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
public class DataStructureB {
	public void cityNameCount(ArrayList<Employee> employees) {
		Set<String> l = new TreeSet<String>();
        List<String> a = new ArrayList<String>();
        for (Employee s : employees) {
           a.add(s.getCity());
           l.add(s.getCity());
           }
        for (String s : l) {
        
        System.out.print(s+"="+Collections.frequency(a,s)+" ");
        
        }
	}
	
	public void monthlySalary(ArrayList<Employee> employees) {
		for (Employee e : employees) {
	        System.out.print(e.getId()+"="+ (e.getSalary()/12)+" ");
		}
		System.out.println();
		for(Employee emp:employees)
		{
	        try {
	        	if(emp.getId()<=0) {
	        	throw new IllegalArgumentException("Id, it can't be 0");
	        	}
	        	if(emp.getName()==null) {
	        		throw new IllegalArgumentException("Name, it can't be null");
	        	}
	        	if(emp.getAge()<=0) {
	        		throw new IllegalArgumentException("Age, it can't be 0");
	        	}
	        	if(emp.getSalary()<=0) {
	        		throw new IllegalArgumentException("Salary, it can't be 0");
	        	}
	        	if(emp.getDept()==null) {
	        		throw new IllegalArgumentException("Department, be can't be null");
	        	}
	        	if(emp.getCity()==null) {
	        		throw new IllegalArgumentException("City, it can't be null");
	        	}
	        	
	        }
	        catch(IllegalArgumentException e)
	        {
	        	System.out.println("Exception raised at "+e.getMessage());
	        }
	   }
	}
}
