package com.hcl.gl.assessment;
public class Main {

	public static void main(String[] args) {
		
			
			    //Extra  displaying super also apart from what assignment is not given**
		        System.out.println("Displaying details of Super department are :");
			    SuperDepartment superdept = new SuperDepartment();
			    System.out.println("Department name is:"+superdept.depName());
			    System.out.println("Todays work progress:"+superdept.getTodaysWork());
			    System.out.println("Deadline:"+superdept.getWorkDeadline());
			    System.out.println("Check whether today is holiday or not:"+superdept.isTodayAHoliday());
			    System.out.println();
		        //Extra part close**
		        
			    //Displaying AdminDepartment details
			    System.out.println("Displaying details of Admin Department are: ");
			    AdminDepartment admindept = new AdminDepartment();
			    System.out.println("Department name is:"+admindept.depName());
			    System.out.println("Todays work progress:"+admindept.getTodaysWork());
			    System.out.println("Deadline:"+admindept.getWorkDeadline());
			    //Calling isTodayAHoliday() of super class through 
			    //sub-class(AdminDepartment) object and 
			    //displaying whether today is holiday or not
			    System.out.println("Check whether today is holiday or not:"+admindept.isTodayAHoliday());
			    System.out.println();
			    
			    //Displaying HR department details
			    System.out.println("Displaying details of HR Department are: ");
			    HrDepartment hr = new HrDepartment();
			    System.out.println("Department name is:"+hr.depName());
			    System.out.println("Todays work progress:"+hr.getTodaysWork());
			    System.out.println("Status activity:"+hr.doactivity());
			    System.out.println("Deadline:"+hr.getWorkDeadline());
			    //Calling isTodayAHoliday() of super class through 
			    //sub-class(HR Department) object and
			    //displaying whether today is holiday or not
			    System.out.println("Check whether today is holiday or not:"+hr.isTodayAHoliday());
			    System.out.println();
			    
			    //Displaying TechDepartment details
			    System.out.println("Displaying details of Tech Department are: ");
			    TechDepartment techdept = new TechDepartment();
			    System.out.println("Department name is:"+techdept.depName());       
			    System.out.println("Todays work progress:"+techdept.getTodaysWork());
			    System.out.println("Tech Stack Information:"+techdept.getTechStackInformation());
			    System.out.println("Deadline:"+techdept.getWorkDeadline());
			    //Calling isTodayAHoliday() of super class through 
			    //sub-class(TechDepartment) object and 
			    //displaying whether today is holiday or not
			    System.out.println("Check whether today is holiday or not:"+techdept.isTodayAHoliday());
			    
			   
			   }
			   

	

}
